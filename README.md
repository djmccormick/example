Example

Requirements:

* PHP 5.3+
* MySQL
* Apache

Installation:

* Import example.sql to a database named 'example'
* Copy config.sample.json to a file named config.json and customize the values
* Ensure Apache will allow the included .htaccess to override what it needs
* Visit the URL where you've installed the app and add 'index.html" to the end

Sample:

* This app is running here: [http://ataensic.net/~djmccormick/example/index.html](http://ataensic.net/~djmccormick/example/index.html)

