<?php

    // Load the API configuration options
    $config = json_decode(file_get_contents('../config.json'), true);

    // Require and configure Flight
    require 'libraries/flight-1.1.5/flight/Flight.php';
    Flight::register('db', 'PDO', array(
        'mysql:host='.$config['db']['host'].';dbname='.$config['db']['name'],
        $config['db']['username'],
        $config['db']['password']
    ));

    // Require Valitron
    require 'libraries/valitron-master/src/Valitron/Validator.php';

    // Determine the base in order to serve out of a subdirectory
    $base = substr(dirname(getenv('SCRIPT_NAME')), 0, -4);


    /*
     * Helpers
     */

    // Fix the data types on a profile array
    $morph = function($profile) {
        // Convert integers to integer values
        foreach (array('id', 'hobby_cycling', 'hobby_frisbee', 'hobby_skiing') as $key) {
            $profile[$key] = intval($profile[$key]);
        }

        return $profile;
    };

    $validate = function($profile, $id) {
        $result = true;

        // Configure Valitron to look at our $profile variable
        $v = new Valitron\Validator($profile);

        // Define the validation rules for a profile
        $v->rule('required', array('firstname', 'lastname', 'email', 'city', 'state', 'sex'));
        $v->rule('lengthBetween', array('firstname', 'lastname', 'city'), 1, 50);
        $v->rule('lengthBetween', 'email', 1, 100);
        $v->rule('lengthBetween', 'comments', 0, 4294967295);
        $v->rule('length', 'state', 2);
        $v->rule('length', 'sex', 1);
        $v->rule('email', 'email');
        $v->rule('integer', array('hobby_frisbee', 'hobby_skiing', 'hobby_cycling'));
        $v->rule('min', array('hobby_frisbee', 'hobby_skiing', 'hobby_cycling'), 0);
        $v->rule('max', array('hobby_frisbee', 'hobby_skiing', 'hobby_cycling'), 1);

        // If id was supplied, ensure it matches in the payload
        if (!is_null($id)) {
            $v->rule('required', 'id');
            $v->rule('in', 'id', array($id));
        }

        // If the validation fails, return the errors
        if(!$v->validate()) {
            $result = $v->errors();
        }

        return $result;
    };


    /*
     * Handle CRUD for Profiles
     */

    // Create
    Flight::route('POST '.$base.'/profiles', function() use ($validate, $morph) {
        $request = Flight::request();
        $response = array();

        // Transform the request from JSON to native PHP
        $requestBody = json_decode($request->body, true);
        if ($requestBody === false) {
            Flight::halt(400);
        }

        // If an id was present on the requestBody, remove it
        if (array_key_exists('id', $requestBody)) {
            unset($requestBody['id']);
        }

        // Run validation
        $validationResult = $validate($requestBody, null);
        if ($validationResult !== true) {
            Flight::halt(400);
        }

        // Create the database record
        $query = Flight::db()->prepare('INSERT INTO profiles VALUES (:id, :firstname, :lastname, :email, :sex, :city, :state, :comments, :hobby_cycling, :hobby_frisbee, :hobby_skiing)');
        $query->bindValue('id', null, PDO::PARAM_INT);
        $query->bindParam('firstname', $requestBody['firstname'], PDO::PARAM_STR);
        $query->bindParam('lastname', $requestBody['lastname'], PDO::PARAM_STR);
        $query->bindParam('email', $requestBody['email'], PDO::PARAM_STR);
        $query->bindParam('sex', $requestBody['sex'], PDO::PARAM_STR);
        $query->bindParam('city', $requestBody['city'], PDO::PARAM_STR);
        $query->bindParam('state', $requestBody['state'], PDO::PARAM_STR);
        $query->bindParam('comments', $requestBody['comments'], PDO::PARAM_STR);
        $query->bindParam('hobby_cycling', $requestBody['hobby_cycling'], PDO::PARAM_INT);
        $query->bindParam('hobby_frisbee', $requestBody['hobby_frisbee'], PDO::PARAM_INT);
        $query->bindParam('hobby_skiing', $requestBody['hobby_skiing'], PDO::PARAM_INT);

        if ($query->execute()) {
            // Return the created profile
            $lastInsertId = Flight::db()->lastInsertId();
            $selectQuery = Flight::db()->prepare('SELECT * FROM profiles WHERE id = :id');
            $selectQuery->bindParam(':id', $lastInsertId);
            $selectQuery->execute();

            foreach($selectQuery->fetchAll(PDO::FETCH_ASSOC) as $row) {
                array_push($response, $morph($row));
            }
        } else {
            Flight::halt(500);
        }

        // Send the response
        if (count($response) > 1) {
            Flight::json($response);
        } else if (count($response) === 1) {
            Flight::json($response[0]);
        } else {
            Flight::halt(500);
        }
    });

    // Read
    Flight::route('GET '.$base.'/profiles', function($id) use ($morph) {
        $response = array();

        // Run the select query
        $query = Flight::db()->prepare('SELECT * FROM profiles ORDER BY lastname ASC');
        $query->execute();

        // Construct the response
        foreach($query->fetchAll(PDO::FETCH_ASSOC) as $row) {
            array_push($response, $morph($row));
        }

        // Send the response
        Flight::json($response);
    });

    Flight::route('GET '.$base.'/profile/@id', function($id) use ($morph) {
        $response = array();

        // Run the select query
        $query = Flight::db()->prepare('SELECT * FROM profiles WHERE id = :id ORDER BY lastname ASC');
        $query->bindParam(':id', intval($id));
        $query->execute();

        foreach($query->fetchAll(PDO::FETCH_ASSOC) as $row) {
            array_push($response, $morph($row));
        }

        // Send the response
        if (count($response) > 1) {
            Flight::json($response);
        } else if (count($response) === 1) {
            Flight::json($response[0]);
        } else {
            Flight::halt(404);
        }
    });

    // Update
    Flight::route('PUT '.$base.'/profile/@id', function($id) use ($validate, $morph) {
        $request = Flight::request();
        $response = array();

        // Transform the request from JSON to native PHP
        $requestBody = json_decode($request->body, true);
        if ($requestBody === false) {
            Flight::halt(400);
        }

        // Run validation
        $validationResult = $validate($requestBody, $id);
        if ($validationResult !== true) {
            Flight::halt(400);
        }

        // Update the database record
        $query = Flight::db()->prepare('UPDATE profiles SET id=:id, firstname=:firstname, lastname=:lastname, email=:email, sex=:sex, city=:city, state=:state, comments=:comments, hobby_cycling=:hobby_cycling, hobby_frisbee=:hobby_frisbee, hobby_skiing=:hobby_skiing WHERE id=:id');
        $query->bindParam('id', $requestBody['id'], PDO::PARAM_INT);
        $query->bindParam('firstname', $requestBody['firstname'], PDO::PARAM_STR);
        $query->bindParam('lastname', $requestBody['lastname'], PDO::PARAM_STR);
        $query->bindParam('email', $requestBody['email'], PDO::PARAM_STR);
        $query->bindParam('sex', $requestBody['sex'], PDO::PARAM_STR);
        $query->bindParam('city', $requestBody['city'], PDO::PARAM_STR);
        $query->bindParam('state', $requestBody['state'], PDO::PARAM_STR);
        $query->bindParam('comments', $requestBody['comments'], PDO::PARAM_STR);
        $query->bindParam('hobby_cycling', $requestBody['hobby_cycling'], PDO::PARAM_INT);
        $query->bindParam('hobby_frisbee', $requestBody['hobby_frisbee'], PDO::PARAM_INT);
        $query->bindParam('hobby_skiing', $requestBody['hobby_skiing'], PDO::PARAM_INT);

        if ($query->execute()) {
            // Return the updated profile
            $selectQuery = Flight::db()->prepare('SELECT * FROM profiles WHERE id = :id');
            $selectQuery->bindParam(':id', $requestBody['id']);
            $selectQuery->execute();

            foreach($selectQuery->fetchAll(PDO::FETCH_ASSOC) as $row) {
                array_push($response, $morph($row));
            }
        } else {
            Flight::halt(500);
        }

        // Send the response
        if (count($response) > 1) {
            Flight::json($response);
        } else if (count($response) === 1) {
            Flight::json($response[0]);
        } else {
            Flight::halt(404);
        }
    });

    // Delete
    Flight::route('DELETE '.$base.'/profile/@id', function($id) {
        $response = array();

        // Run the delete query
        $query = Flight::db()->prepare('DELETE FROM profiles WHERE id = :id');
        $query->bindParam('id', intval($id), PDO::PARAM_INT);
        $query->execute();

        // Determine how many rows were affected by our delete query
        $affected = $query->rowCount();

        // Send the response
        if ($affected === 1) {
            Flight::halt(204);
        } else {
            Flight::halt(404);
        }
    });


    /*
     * Start the server
     */

    Flight::start();

?>
