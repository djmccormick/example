// Configure RequireJS:
requirejs.config({
    baseUrl: 'app/libraries',
    paths: {
        jquery: 'jquery-1.11.0',
        underscore: 'underscore-1.6.0',
        backbone: 'backbone-1.1.2',
        bootstrap: 'bootstrap-3.1.1/js/bootstrap',
        jqBootstrapValidation: 'jqBootstrapValidation-1.3.7',
        collections: '../collections',
        models: '../models',
        views: '../views',
        routers: '../routers',
        templates: '../templates'
    },
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        bootstrap: {
            deps: ['jquery'],
            exports: '$.fn.popover'
        },
        jqBootstrapValidation: {
            deps: ['jquery', 'bootstrap'],
            exports: '$.fn.jqBootstrapValidation'
        }
    }
});

// Run the app:
define(function(require) {

    'use strict';

    require('bootstrap');
    require('jqBootstrapValidation');
    var Backbone = require('backbone');
    var MainRouter = require('routers/main');

    window.app = new MainRouter();

    Backbone.history.start();

});
