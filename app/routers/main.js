define(function(require) {

    'use strict';

    var $ = require('jquery');
    var _ = require('underscore');
    var Backbone = require('backbone');

    // Views
    var AppView = require('views/app');

    return Backbone.Router.extend({
        views: {},
        routes: {
            '': 'index',
            'profiles': 'index',
            'profiles/:id': 'index'
        },
        index: function(id) {
            if (!this.views.hasOwnProperty('app')) {
                this.views.app = new AppView({
                    el: $('body'),
                    selectedProfileId: id
                });

                this.views.app.render();
            } else {
                this.views.app.setSelectedProfile(id);
            }
        }
    });

});
