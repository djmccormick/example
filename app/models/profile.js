define(function(require) {

    var $ = require('jquery');
    var _ = require('underscore');
    var Backbone = require('backbone');

    return Backbone.Model.extend({
        url: function () {
            var result = 'profiles';

            if (this.get('id')) {
                result = result.slice(0, - 1) + '/' + this.get('id');
            }

            return result;
        }
    });

});
