define(function(require) {

    var $ = require('jquery');
    var _ = require('underscore');
    var Backbone = require('backbone');

    var ProfileModel = require('models/profile');

    return Backbone.Collection.extend({
        model: ProfileModel,
        comparator: 'lastname',
        url: function () {
            return 'profiles';
        }
    });

});
