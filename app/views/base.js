define(function(require) {

    var $ = require('jquery');
    var _ = require('underscore');
    var Backbone = require('backbone');

    return Backbone.View.extend({
        initialize: function (options) {
            this.options = options;
            this.collections = {};
            this.models = {};
            this.views = {};
        },
        render: function() {
            this.$el.html(this.template());

            return this;
        },
        show: function () {
            this.trigger('show');
            this.$el.show();

            return this;
        },
        hide: function () {
            this.trigger('hide');
            this.$el.hide();

            return this;
        }
    });

});
