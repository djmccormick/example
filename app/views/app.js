define(function(require) {

    'use strict';

    var $ = require('jquery');
    var _ = require('underscore');
    var Backbone = require('backbone');

    // Views
    var BaseView = require('views/base');
    var ProfileView = require('views/profile');
    var EditorView = require('views/editor');

    // Collections
    var ProfilesCollection = require('collections/profiles');

    // Templates
    var template = require('text!templates/app.html');
    var profileTemplate = require('text!templates/profile.html');

    return BaseView.extend({
        selectedProfileId: undefined,
        template: _.template(template),
        profileTemplate: _.template(profileTemplate),
        initialize: function(options) {
            BaseView.prototype.initialize.call(this);

            // Set up an array to house the profile views
            this.views.profiles = [];

            // Save the options
            this.options = options;

            // Save the profiles collection
            this.collections.profiles = new ProfilesCollection();
            this.collections.profiles.on('add', this.onProfileAddedToCollection, this);

            return this;
        },
        render: function() {
            // Render the template
            this.$el.html(this.template());

            // Cache the list group selector
            this.$listGroup = this.$el.find('.col-md-3 > .list-group-container > .list-group');

            // Handle window resizing
            this.resizeFun = _.bind(this.onResize, this);
            this.resizeFun();
            $(window).resize(this.resizeFun);

            // Fetch the profiles
            this.collections.profiles.fetch({
                success: _.bind(this.onProfilesFetchSuccess, this)
            });

            return this;
        },
        onResize: function() {
            // Adjust the navigation height
            var viewportHeight = $(window).height();
            var navHeight = $('body > .navbar').height();
            this.$target = this.$target || $('body > .container-fluid > .row > .col-md-3 > .list-group-container');
            this.$target.height(viewportHeight - navHeight - 2);
        },
        onProfilesFetchSuccess: function() {
            // Select the appropriate profile
            this.setSelectedProfile(this.options.selectedProfileId);
        },
        setSelectedProfile: function(id) {
            // If no id was provided, either select the first entry or (if there are no entries) create a new one
            if ((_.isUndefined(id) || _.isNull(id)) && this.collections.profiles.length) {
                id = this.collections.profiles.at(0).get('id');
            } else if (_.isUndefined(id) || _.isNull(id)) {
                id = 'new';
            }

            // Keep track of the selected profile id
            this.selectedProfileId = id;

            // Ensure the URL reflects which id we're viewing
            window.app.navigate('profiles/' + this.selectedProfileId);

            // Set up and render the editor
            this.$editorContainer = this.$editorContainer || this.$el.find('.col-md-9');
            if (this.views.hasOwnProperty('editor')) {
                this.views.editor.destroy();
            }
            this.views.editor = new EditorView({ el: this.$editorContainer, selectedProfile: this.collections.profiles.get(id), profilesCollection: this.collections.profiles });
            this.views.editor.render();
            this.views.editor.on('created', this.onProfileCreated, this);
            this.views.editor.on('deleted', this.onProfileDeleted, this);

            // Highlight the appropriate profile
            this.$el.find('.list-group-item').removeClass('active');
            this.$currentlySelectedItem = this.$el.find('.list-group-item[data-id=' + id + ']');
            this.$currentlySelectedItem.addClass('active');

            // Scroll to the appropriate profile
            if (!_.isUndefined(this.$currentlySelectedItem.position())) {
                var scrollTopValue = this.$listGroup.parent().scrollTop() + this.$currentlySelectedItem.position().top;
                this.$listGroup.parent().animate({
                    scrollTop: scrollTopValue
                }, 1000);
            }
        },
        onProfileAddedToCollection: function(profile) {
            // Create and render a view to represent this profile
            this.views.profiles[profile.get('id')] = new ProfileView({
                profileModel: profile,
                el: $('<a class="list-group-item"></a>').appendTo(this.$listGroup)
            }).render();

            // Add the view to the DOM
            var index = this.collections.profiles.indexOf(profile);

            if (index === 0) {
                this.$listGroup.prepend(this.views.profiles[profile.get('id')].el);
            } else {
                var $sibling = this.$listGroup.children(':nth-child(' + index + ')');
                $sibling.after(this.views.profiles[profile.get('id')].el);
            }
        },
        onProfileCreated: function (profile) {
            // Select the newly created profile
            this.setSelectedProfile(profile.get('id'));
        },
        onProfileDeleted: function (profileId) {
            // Destroy the view
            this.views.profiles[profileId].destroy();

            // Delete the view from the internal array
            this.views.profiles.splice(this.views.profiles.indexOf(profileId), 1);

            // Select the default profile
            this.setSelectedProfile();
        },
        destroy: function () {
            // Destroy any events
            this.undelegateEvents();
            $(window).off('resize', this.resizeFun);
        }
    });

});
