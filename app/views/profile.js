define(function(require) {

    'use strict';

    var $ = require('jquery');
    var _ = require('underscore');
    var Backbone = require('backbone');

    // Views
    var BaseView = require('views/base');

    // Templates
    var template = require('text!templates/profile.html');

    return BaseView.extend({
        template: _.template(template),
        initialize: function(options) {
            BaseView.prototype.initialize.call(this);

            // Save the options
            this.options = options;

            // Save the profile model
            this.models.profile = this.options.profileModel;
            this.models.profile.on('change', this.render, this);

            return this;
        },
        render: function() {
            // Add an href and data-id attribute to this element
            this.$el.attr('href', '#profiles/' + this.models.profile.get('id'));
            this.$el.attr('data-id', this.models.profile.get('id'));

            // Render the template
            this.$el.html(this.template({
                profile: this.models.profile
            }));

            return this;
        },
        destroy: function (navigate) {
            // Destroy any events
            this.undelegateEvents();

            // Hide then destroy this element
            this.$el.slideUp('slow', _.bind(function() {
                this.$el.remove();
            }, this));
        }
    });

});
