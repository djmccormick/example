define(function(require) {

    'use strict';

    var $ = require('jquery');
    var _ = require('underscore');
    var Backbone = require('backbone');

    // Views
    var BaseView = require('views/base');

    // Models
    var ProfileModel = require('models/profile');

    // Templates
    var template = require('text!templates/editor.html');

    return BaseView.extend({
        template: _.template(template),
        isNew: false,
        profileId: undefined,
        events: {
            'click .confirmed-delete': 'onClickDelete'
        },
        initialize: function(options) {
            BaseView.prototype.initialize.call(this);

            _.bindAll(this, 'onClickDelete', 'onClickSave');

            // Save the profile model
            if (options.selectedProfile) {
                this.models.profile = options.selectedProfile;
            } else {
                this.isNew = true;
                this.models.profile = new ProfileModel();
            }

            // Save the profiles collection
            this.collections.profiles = options.profilesCollection;

            return this;
        },
        render: function() {
            // Render the template
            this.$el.html(this.template({
                profile: this.models.profile
            }));

            // Initialize validation
            this.$el.find('form input, form textarea').jqBootstrapValidation({
                preventSubmit: true,
                submitSuccess: _.bind(this.onClickSave, this)
            });

            // Handle window resizing
            this.resizeFun = _.bind(this.onResize, this);
            this.resizeFun();
            $(window).resize(this.resizeFun);

            return this;
        },
        onResize: function() {
            var viewportHeight = $(window).height();
            var navHeight = $('body > .navbar').height();
            this.$target = this.$target || this.$el.find('.panel-default > .panel-body');
            this.$target.height(viewportHeight - navHeight - 172);
        },
        destroy: function() {
            // Destroy any events
            this.undelegateEvents();
            $(window).off('resize', this.resizeFun);
        },
        onClickDelete: function() {
            this.$el.find('.modal .modal-footer button').attr('disabled', 'disabled');
            this.models.profile.destroy({
                success: _.bind(this.onDeleteSuccess, this)
            });
        },
        onDeleteSuccess: function(model) {
            // Hide the modal
            this.$el.find('.modal').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();

            // Communicate the deletion
            this.trigger('deleted', model.get('id'));
        },
        onClickSave: function($el, event) {
            // Disable 'Save' and 'Delete' buttons
            this.$el.find('.panel-footer button').attr('disabled', 'disabled');

            // Collect our hobby values
            var checkedHobbies = [];
            this.$el.find('input[name=hobbies]:checked').each(function (index, el) {
                checkedHobbies.push($(el).val());
            });

            // Place the form values into the model and save
            this.models.profile.save(
                {
                    firstname: $.trim(this.$el.find('#firstname').val()),
                    lastname: $.trim(this.$el.find('input[name=lastname]').val()),
                    email: $.trim(this.$el.find('input[name=email]').val()),
                    sex: $.trim(this.$el.find('input[name=sex]:checked').val()),
                    city: $.trim(this.$el.find('input[name=city]').val()),
                    state: $.trim(this.$el.find('input[name=state]').val()),
                    comments: $.trim(this.$el.find('textarea[name=comments]').val()),
                    hobby_cycling: checkedHobbies.indexOf('cycling') !== -1 ? 1 : 0,
                    hobby_frisbee: checkedHobbies.indexOf('frisbee') !== -1 ? 1 : 0,
                    hobby_skiing: checkedHobbies.indexOf('skiing') !== -1 ? 1 : 0
                },
                {
                    success: _.bind(this.onSaveSuccess, this)
                }
            );

            // Prevent a true form submission
            event.preventDefault();
        },
        onSaveSuccess: function() {
            // Enable 'Save' and 'Delete' buttons
            this.$el.find('.panel-footer button').removeAttr('disabled');

            // Add the new model to the collection, if necessary and trigger an event
            if (this.isNew) {
                this.collections.profiles.add(this.models.profile);
                this.trigger('created', this.models.profile);
            } else {
                this.trigger('updated', this.models.profile);
            }
        }
    });

});
