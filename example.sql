-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 25, 2014 at 11:36 PM
-- Server version: 5.5.35
-- PHP Version: 5.3.10-1ubuntu3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `example`
--

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE IF NOT EXISTS `profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `sex` varchar(1) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state` varchar(2) DEFAULT NULL,
  `comments` longtext,
  `hobby_cycling` int(11) DEFAULT NULL,
  `hobby_frisbee` int(11) DEFAULT NULL,
  `hobby_skiing` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=163 ;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `firstname`, `lastname`, `email`, `sex`, `city`, `state`, `comments`, `hobby_cycling`, `hobby_frisbee`, `hobby_skiing`) VALUES
(34, 'Ernie', 'Stenseth', 'ernie_stenseth@aol.com', 'M', 'Ridgefield Park', 'NJ', '', 0, 1, 0),
(35, 'Albina', 'Glicks', 'albina@glick.com', 'F', 'Dunellens', 'NJ', 'This is a thing.', 0, 1, 1),
(40, 'Valentine', 'Gillian', 'valentine_gillian@gmail.com', 'M', 'San Antonio', 'TX', '', 0, 0, 0),
(41, 'Kati', 'Rulapaugh', 'kati.rulapaugh@hotmail.com', 'F', 'Abilene', 'KS', '', 0, 0, 1),
(42, 'Youlanda', 'Schemmer', 'youlanda@aol.com', 'F', 'Prineville', 'OR', '', 1, 0, 1),
(45, 'Lavera', 'Perin', 'lperin@perin.org', 'F', 'Miami', 'FL', '', 1, 0, 1),
(46, 'Erick', 'Ferencz', 'erick.ferencz@aol.com', 'M', 'Fairbanks', 'AK', '', 0, 1, 0),
(48, 'Jina', 'Briddick', 'jina_briddick@briddick.com', 'F', 'Boston', 'MA', '', 1, 0, 1),
(49, 'Kanisha', 'Waycott', 'kanisha_waycott@yahoo.com', 'F', 'Los Angeles', 'CA', '', 1, 1, 0),
(50, 'Emerson', 'Bowley', 'emerson.bowley@bowley.org', 'F', 'Madison', 'WI', '', 0, 0, 0),
(53, 'Josephine', 'Darakjy', 'josephine_darakjy@darakjy.org', 'F', 'Brighton', 'MI', '', 1, 0, 1),
(54, 'Art', 'Venere', 'art@venere.org', 'M', 'Bridgeport', 'NJ', '', 0, 1, 0),
(55, 'Lenna', 'Paprocki', 'lpaprocki@hotmail.com', 'F', 'Anchorage', 'AK', '', 0, 0, 0),
(56, 'Donette', 'Foller', 'donette.foller@cox.net', 'F', 'Hamilton', 'OH', '', 0, 0, 0),
(57, 'Simona', 'Morasca', 'simona@morasca.com', 'F', 'Ashland', 'OH', '', 1, 0, 0),
(58, 'Mitsue', 'Tollner', 'mitsue_tollner@yahoo.com', 'F', 'Chicago', 'IL', '', 0, 1, 0),
(59, 'Leota', 'Dilliard', 'leota@hotmail.com', 'F', 'San Jose', 'CA', '', 0, 0, 0),
(60, 'Sage', 'Wieser', 'sage_wieser@cox.net', 'F', 'Sioux Falls', 'SD', '', 0, 0, 0),
(61, 'Kris', 'Marrier', 'kris@gmail.com', 'M', 'Baltimore', 'MD', '', 0, 0, 0),
(62, 'Minna', 'Amigon', 'minna_amigon@yahoo.com', 'F', 'Kulpsville', 'PA', '', 1, 1, 0),
(63, 'Abel', 'Maclead', 'amaclead@gmail.com', 'M', 'Middle Island', 'NY', '', 0, 1, 1),
(65, 'Graciela', 'Ruta', 'gruta@cox.net', 'F', 'Chagrin Falls', 'OH', '', 0, 0, 1),
(66, 'Cammy', 'Albares', 'calbares@gmail.com', 'F', 'Laredo', 'TX', '', 1, 0, 1),
(67, 'Mattie', 'Poquette', 'mattie@aol.com', 'F', 'Phoenix', 'AZ', '', 1, 0, 0),
(68, 'Meaghan', 'Garufi', 'meaghan@hotmail.com', 'F', 'Mc Minnville', 'TN', '', 1, 0, 0),
(69, 'Gladys', 'Rim', 'gladys.rim@rim.org', 'F', 'Milwaukee', 'WI', '', 0, 1, 0),
(71, 'Fletcher', 'Flosi', 'fletcher.flosi@yahoo.com', 'M', 'Rockford', 'IL', '', 0, 0, 1),
(73, 'Veronika', 'Inouye', 'vinouye@aol.com', 'F', 'San Jose', 'CA', '', 0, 0, 0),
(74, 'Willard', 'Kolmetz', 'willard@hotmail.com', 'M', 'Irving', 'TX', '', 0, 1, 0),
(75, 'Maryann', 'Royster', 'mroyster@royster.com', 'F', 'Albany', 'NY', '', 0, 1, 0),
(76, 'Alisha', 'Slusarski', 'alisha@slusarski.com', 'F', 'Middlesex', 'NJ', '', 0, 0, 1),
(77, 'Allene', 'Iturbide', 'allene_iturbide@cox.net', 'F', 'Stevens Point', 'WI', '', 1, 1, 1),
(78, 'Chanel', 'Caudy', 'chanel.caudy@caudy.org', 'F', 'Shawnee', 'KS', '', 0, 1, 1),
(79, 'Ezekiel', 'Chui', 'ezekiel@chui.com', 'M', 'Easton', 'MD', '', 1, 0, 1),
(80, 'Willow', 'Kusko', 'wkusko@yahoo.com', 'F', 'New York', 'NY', '', 1, 0, 0),
(81, 'Bernardo', 'Figeroa', 'bfigeroa@aol.com', 'M', 'Conroe', 'TX', '', 0, 0, 1),
(82, 'Ammie', 'Corrio', 'ammie@corrio.com', 'F', 'Columbus', 'OH', '', 1, 0, 0),
(83, 'Francine', 'Vocelka', 'francine_vocelka@vocelka.com', 'F', 'Las Cruces', 'NM', '', 0, 1, 0),
(84, 'Ernie', 'Stenseth', 'ernie_stenseth@aol.com', 'M', 'Ridgefield Park', 'NJ', '', 0, 1, 0),
(86, 'Alishia', 'Sergi', 'asergi@gmail.com', 'F', 'New York', 'NY', '', 0, 1, 1),
(87, 'Solange', 'Shinko', 'solange@shinko.com', 'M', 'Metairie', 'LA', '', 1, 1, 0),
(88, 'Jose', 'Stockham', 'jose@yahoo.com', 'M', 'New York', 'NY', '', 0, 1, 0),
(89, 'Rozella', 'Ostrosky', 'rozella.ostrosky@ostrosky.com', 'F', 'Camarillo', 'CA', '', 1, 0, 0),
(93, 'Dyan', 'Oldroyd', 'doldroyd@aol.com', 'M', 'Overland Park', 'KS', '', 0, 0, 1),
(94, 'Roxane', 'Campain', 'roxane@hotmail.com', 'F', 'Fairbanks', 'AK', '', 0, 1, 1),
(95, 'Lavera', 'Perin', 'lperin@perin.org', 'F', 'Miami', 'FL', '', 1, 0, 1),
(97, 'Fatima', 'Saylors', 'fsaylors@saylors.org', 'F', 'Hopkins', 'MN', '', 1, 1, 0),
(99, 'Kanisha', 'Waycott', 'kanisha_waycott@yahoo.com', 'F', 'Los Angeles', 'CA', '', 1, 1, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
